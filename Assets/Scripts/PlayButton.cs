﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayButton : MonoBehaviour
{
    public GameObject LevelMenu;
    public GameObject MainMenu;

    public void OnClick()
    {
        MainMenu.SetActive(false);
        LevelMenu.SetActive(true);
    }
}
