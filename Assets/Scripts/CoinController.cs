﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour
{
    private Animator amin;
    // Start is called before the first frame update
    void Start()
    {
        amin = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        amin.SetTrigger("Collect");
        Destroy(gameObject, 0.6f);
    }
}
