﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    CharacterController characterController;

    [SerializeField] private float Speed = 6;
    [SerializeField] private float RunSpeed = 11;
    [SerializeField] private float JumpSpeed = 8;
    [SerializeField] private float Gravity = 20;

    private bool isRunning;
    private Vector3 moveDirection;

    // Start is called before the first frame update
    void Start()
    {
        TryGetComponent(out characterController);
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 inputs = Vector2.ClampMagnitude(new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")), 1);
        isRunning = Input.GetKey(KeyCode.LeftShift);

        if (isRunning)
        {
            moveDirection = new Vector3(inputs.x * RunSpeed, moveDirection.y, inputs.y * RunSpeed );
        } else
        {
            moveDirection = new Vector3(inputs.x * Speed, moveDirection.y, inputs.y * Speed);
        }

        if (characterController.isGrounded)
        {
            moveDirection.y = 0;

            if(Input.GetButtonDown("Jump"))
            {
                moveDirection.y = JumpSpeed;
            }
        }

        moveDirection.y -= Gravity * Time.deltaTime;
        characterController.Move(transform.TransformVector(moveDirection) * Time.deltaTime);
    }
}
