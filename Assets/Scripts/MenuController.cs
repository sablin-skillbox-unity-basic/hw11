﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    public GameObject LevelMenu;
    public GameObject MainMenu;

    public Button _nurglLevelButton;
    public Button _khornLevelButton;
    public Button _tzinchLevelButton;
    public Button _slaaneshLevelButton;
    public Button _ynneadLevelButton;
    // Start is called before the first frame update
    void Awake()
    {
        _nurglLevelButton.onClick.AddListener(() => ButtonClicked(SceneLoader.Scene.LevelNurgl));
        _khornLevelButton.onClick.AddListener(() => ButtonClicked(SceneLoader.Scene.LevelKhorn));
        _tzinchLevelButton.onClick.AddListener(() => ButtonClicked(SceneLoader.Scene.LevelTzinch));
        _slaaneshLevelButton.onClick.AddListener(() => ButtonClicked(SceneLoader.Scene.LevelSlaanesh));
        _ynneadLevelButton.onClick.AddListener(() => ButtonClicked(SceneLoader.Scene.LevelYnnead));
    }

    public void OnClickBackButton()
    {
        MainMenu.SetActive(true);
        LevelMenu.SetActive(false);
    }

    public void OnClickPlayButton()
    {
        MainMenu.SetActive(false);
        LevelMenu.SetActive(true);
    }

    public void OnClickExitButton()
    {
        Application.Quit();
    }

    private void ButtonClicked(SceneLoader.Scene scene)
    {
        SceneLoader.Load(scene);
    }
}
