﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimController : MonoBehaviour
{
    public enum AnimState
    {
        UpAndDown,
        Grow,
        Shift
    }

    private Animator anim;
    private System.Random random;

    private void Awake()
    {
        TryGetComponent(out anim);
        random = new System.Random();
    }

    public void SwitchAnimation()
    {
        anim.SetInteger("AnimState", random.Next(0, 3));
    }
}
