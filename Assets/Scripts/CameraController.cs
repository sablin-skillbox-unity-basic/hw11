﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Vector2 lookDirection;
    public float Sensitivity = 2.5f;
    public float MinVerticalAngle = -90;
    public float MaxVerticalAngle = 90;

    public Transform HorizontalRotation;
    public Transform VerticalRotation;
    // Start is called before the first frame update
    void Start()
    {
        lookDirection.x = HorizontalRotation.localEulerAngles.y;
        lookDirection.y = VerticalRotation.localEulerAngles.x;
    }

    // Update is called once per frame
    void Update()
    {
        lookDirection = new Vector2(lookDirection.x + Input.GetAxis("Mouse X") * Sensitivity, Mathf.Clamp(lookDirection.y - Input.GetAxis("Mouse Y") * Sensitivity, MinVerticalAngle, MaxVerticalAngle));
        VerticalRotation.localRotation = Quaternion.Euler(lookDirection.y, VerticalRotation.localEulerAngles.y, VerticalRotation.localEulerAngles.z);
        HorizontalRotation.localRotation = Quaternion.Euler(HorizontalRotation.localEulerAngles.x, lookDirection.x, HorizontalRotation.localEulerAngles.z);
    }
}
