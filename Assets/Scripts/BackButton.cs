﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackButton : MonoBehaviour
{
    public GameObject LevelMenu;
    public GameObject MainMenu;

    public void OnClick()
    {
        MainMenu.SetActive(true);
        LevelMenu.SetActive(false);
    }
}
