﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseController : MonoBehaviour
{
    [SerializeField] private GameObject _panelMenu;
    [SerializeField] private Button _pauseButton;

    private void Awake()
    {
        _pauseButton.onClick.AddListener(() => OnClickPause());
    }

    public void OnClickPause()
    {
        _panelMenu.SetActive(true);
        _pauseButton.interactable = false;
        Time.timeScale = 0;
    }

    public void OnClickContinue()
    {
        Time.timeScale = 1;
        _panelMenu.SetActive(false);
        _pauseButton.interactable = true;
    }

    public void OnClickMainMenu()
    {
        SceneLoader.Load(SceneLoader.Scene.MainMenu);
    }
}
